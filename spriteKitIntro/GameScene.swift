//
//  GameScene.swift
//  spriteKitIntro
//
//  Created by Gurlagan Bhullar on 2019-09-30.
//  Copyright © 2019 Gurlagan Bhullar. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    //MARK: Sprite variable
    
    var image : SKSpriteNode!
    var square : SKSpriteNode!
    var highScoreLabel : SKLabelNode!
    
    
    
    //construtor setup your scene + sprites
    override func didMove(to view: SKView) {
        //size is global variable that can get width and height by string interpolation
        print("Screen size: \(size.width), \(size.height)")
        
        
        //making a label in spriteKit
        
        self.highScoreLabel = SKLabelNode(text: "score: ")
        
        //configure the node by setting the size.color.positio
        self.highScoreLabel.position = CGPoint(x: 100, y: 100)
        self.highScoreLabel.fontSize = 45
        self.highScoreLabel.fontColor = UIColor.yellow
        self.highScoreLabel.fontName = "Avenir"
        
        addChild(highScoreLabel)
        
        
        // MARK: : Draw a Square
        
        //CGSIze objects = react ohject in java
        //create a square first
        self.square = SKSpriteNode(color: UIColor.yellow, size: CGSize(width: 100, height: 100))
        //confuggre the square
        self.square.position = CGPoint(x: 100, y: 200)
        
        //add the square to the screen
        addChild(square)
        
        
        //image add
        
        self.image = SKSpriteNode(imageNamed: "pikachu64")
        self.image.position = CGPoint(x: 200, y: 300)
        addChild(self.image)
        
    }
    var imageDirection = "right"
    //updatePostions
    override func update(_ currentTime: TimeInterval) {
        
        //built in func that runs once per frame similar to updateposition in android
      //  print("hello")
    //types of movement ,manual movement or automatic movement
        
       // self.image.position.x = self.image.position.x + 1
        
        if(self.imageDirection == "right"){
            self.image.position.x = self.image.position.x + 10
            if(self.image.position.x >= size.width)
            {
                self.imageDirection = "left"
            }
            
        }
        else if(self.imageDirection == "left"){
             self.image.position.x = self.image.position.x - 10
            if(self.image.position.x <= 0 ){
                self.imageDirection = "right"
            }
        }
        //Automated Movement
//        let moveAction = SKAction.moveBy(x: 0, y: 1, duration: 2)
//        self.square.run(moveAction)
//
//
//        self.highScoreLabel.run(moveAction)
//
        
        //use automatic movement by the sequence
        
        let  moveAction = SKAction.moveBy(x: 0, y: 1, duration: 2)
        let leftmoveAction = SKAction.moveBy(x: -1, y: 0, duration: 2)
         let rightmoveAction = SKAction.moveBy(x: 100, y: 0, duration: 2)
        
        let animation = SKAction.sequence([moveAction,leftmoveAction,rightmoveAction])
        self.square.run(animation)
    }
    //user input function and in built function
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      //
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("person touched the screen")
        
        //detects X and Y positon on finger
        let locationTOuched = touches.first
        if(locationTOuched == nil){
            
        }
        
        let mousePosition = locationTOuched!.location(in: self)
        print("x + \(mousePosition.x)")
         print("y + \(mousePosition.y)")
        print("_________")
        
    }
}
